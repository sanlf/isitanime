#!/bin/bash

#The images belong to the mirflickr dataset, which can be found at:
#http://press.liacs.nl/mirflickr/mirdownload.html
#I specifically just needed around 30k images but the rest can be found at:
#http://press.liacs.nl/mirflickr/mirflickr1m.v2/

mkdir -p non_anime_images
mkdir -p flickr_images

#Download the files
wget -c http://press.liacs.nl/mirflickr/mirflickr1m.v2/images0.zip -P non_anime_images

#Decompress
unzip images0.zip -d flickr_images

#Takes all images in unzipped folders and move them to images folder
find flick_images -type f -exec mv -v {} non_anime_images \;

#Remove the now empty folder flick_images
rm -r flick_images

