let base64Image;

//////////////////////////////////////////////////////////////////////////////

$("#upload-file").change(function(){
    let reader = new FileReader();

    reader.onload = function(e){
        let dataURL = reader.result;
        $('#preview').attr("src", dataURL);
        base64Image = dataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
        console.log(base64Image);

        let message = {
            image: base64Image
        }
        console.log(message);
        $.post("http://localhost:5000/predict", JSON.stringify(message), function(response){
            $("#message").text(response.prediction.anime.toFixed(2) + "% Anime");
            console.log(response);
        });
    }

    reader.readAsDataURL($("#upload-file")[0].files[0]);
    $("#message").text("Classifying...");
});

//////////////////////////////////////////////////////////////////////////////

$("#upload-url").change(function(){
    let reader = new FileReader();

    reader.onload = function(e){
        let dataURL = reader.result;
        $('#preview').attr("src", dataURL);
        base64Image = dataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
        console.log(base64Image);

        let message = {
            image: base64Image
        }
        console.log(message);
        $.post("http://localhost:5000/predict", JSON.stringify(message), function(response){
            $("#message").text(response.prediction.anime.toFixed(2) + "% Anime");
            console.log(response);
        });
    }

    reader.readAsDataURL($("#upload-url")[0].files[0]);
    $("#message").text("Classifying...");
});

//////////////////////////////////////////////////////////////////////////////

function predict(selected_image){
    let message = {
        image: selected_image
    }
    console.log(message);
    $.post("http://localhost:5000/predict", JSON.stringify(message), function(response){
        $("#anime-prediction").text(response.prediction.anime.toFixed(2) + "%");
        console.log(response);
    });
    console.log(response.prediction.anime.toFixed(2) + "%");
};

//////////////////////////////////////////////////////////////////////////////

