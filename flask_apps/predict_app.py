import base64
import numpy as np
import io
from PIL import Image
import keras
from keras import backend as K
from keras.models import Sequential
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img

from flask import request
from flask import jsonify
from flask import Flask

import sys
sys.path.append("../training/")
from my_models import load_my_model
from prediction import preprocess_image

app = Flask(__name__)

def get_model():
    print(" * Loading keras model...")
    # model_name = "anime_model.h5"
    model_name = "anime_modelvgg16.h5"
    model = load_model(model_name)

    print("Loaded model: " + model_name)

    return model

@app.route('/predict', methods = ['POST'])
def predict():
    K.clear_session() #clear session to be able to predict
                      #because of a tensorflow error
    model = get_model()

    message = request.get_json(force = True)
    encoded = message['image']
    decoded = base64.b64decode(encoded)
    processed_image = preprocess_image(io.BytesIO(decoded))

    prediction = model.predict_proba(processed_image).tolist()
    anime_prediction = prediction[0][0]

    response = {
            'prediction':{
                'anime': anime_prediction*100,
            }
    }

    return jsonify(response)

