from keras import models
from keras import optimizers
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout, Activation, BatchNormalization
from keras.applications import VGG16, VGG19

import os

image_size = 224

def create_model():
    model = models.Sequential()

    model.add(Conv2D(
        filters=20,
        kernel_size=(5, 5),
        strides=(3, 3),
        activation='relu',
        input_shape=(image_size, image_size, 3))
    )
    model.add(Conv2D(filters=20, kernel_size=(3, 3)))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(rate=0.5))

    model.add(Conv2D(filters=50, kernel_size=(3, 3)))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(filters=50, kernel_size=(3, 3)))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(rate=0.5))

    model.add(Conv2D(filters=100, kernel_size=(5, 5)))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(filters=120, kernel_size=(3, 3)))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(filters=150, kernel_size=(3, 3)))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Dropout(rate=0.5))

    model.add(Flatten())
    model.add(Dense(units=1024))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dropout(rate=0.5))

    model.add(Dense(units=1024))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dropout(rate=0.5))

    model.add(Dense(units=1024))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dropout(rate=0.5))

    model.add(Dense(units=2))
    model.add(BatchNormalization())
    model.add(Activation('softmax'))

    return model

##############################################################################

def create_model_vgg16_base():
    vgg_conv = VGG16(
        weights='imagenet',
        include_top=False,
        input_shape=(image_size, image_size, 3)
    )

    # Freeze the vgg16 layers
    for layer in vgg_conv.layers:
        layer.trainable = False

    # Create the model
    model = models.Sequential()

    # Add the vgg convolutional base model
    model.add(vgg_conv)

    # Add new layers
    model.add(Flatten())
    model.add(Dense(40))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dropout(rate=0.5))
    model.add(Dense(2))
    model.add(BatchNormalization())
    model.add(Activation('softmax'))

    return model

##############################################################################

def create_model_vgg19_base():
    vgg_conv = VGG19(
        weights='imagenet',
        include_top=False,
        input_shape=(image_size, image_size, 3)
    )

    # Freeze the vgg19 layers
    for layer in vgg_conv.layers:
        layer.trainable = False

    # Create the model
    model = models.Sequential()

    # Add the vgg convolutional base model
    model.add(vgg_conv)

    # Add new layers
    model.add(Flatten())
    model.add(Dense(40))
    model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Dropout(rate=0.5))
    model.add(Dense(2))
    model.add(BatchNormalization())
    model.add(Activation('softmax'))

    return model

##############################################################################

def load_my_model(name):
    directory = "../models/"
    model_name = "anime_model.h5"

    #if the name has h5 extension, use that model name
    if ".h5" in name:
        model_name = name

    elif name is not None:
        if name == 'vgg16':
            print("Will use vgg16 model")
            model_name = "anime_modelvgg16.h5"
        elif name == 'vgg19':
            print("Will use vgg19 model")
            model_name = "anime_modelvgg19.h5"
        else:
            print("Will use base model")

    if os.path.isfile(directory + model_name):
        print("Loading model: " + model_name)

        # Because of a keras error, when fine tuning the vgg16 and 19 models, the
        # architecture isn't saved so the model must be created and then load the
        # weight
        if model_name == "anime_modelvgg16.h5":
            model = create_model_vgg16_base()
            model.load_weights(directory + model_name)
        elif model_name == "anime_modelvgg19.h5":
            model = create_model_vgg19_base()
            model.load_weights(directory + model_name)
        else:
            model = models.load_model(directory + model_name)

        # Compile the model if it's a vgg one
        if model_name in ["anime_modelvgg16.h5", "anime_modelvgg19.h5"]:
            # Compile the model
            model = compile_model(model)
    else:
        model = None

    return model_name, model

##############################################################################

def compile_model(model):
    optim = optimizers.Adam(
        lr=0.001,
        beta_1=0.9,
        beta_2=0.999,
        epsilon=None,
        decay=1e-6,
        amsgrad=False
    )

    model.compile(**{
        'loss': 'categorical_crossentropy',
        'optimizer': optim,
        'metrics': [
            'accuracy',
        ],
    })

    return model

##############################################################################

