from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator, load_img
from keras.applications import VGG16
from keras.callbacks import ModelCheckpoint

from sklearn.metrics import classification_report, confusion_matrix
from sklearn.utils import class_weight

import matplotlib.pyplot as plt
import numpy as np

import sys

from my_models import create_model, create_model_vgg16_base, create_model_vgg19_base, load_my_model, compile_model

##############################################################################

image_size = 224
batch_size = 32

# Change the batchsize according to your system RAM
train_batchsize = batch_size
val_batchsize = batch_size

train_datagen = ImageDataGenerator(rescale=1./255)

validation_datagen = ImageDataGenerator(rescale=1./255)

train_dir = "../data/train"
train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(image_size, image_size),
        batch_size=train_batchsize,
        class_mode='categorical'
)

validation_dir = "../data/validation"
validation_generator = validation_datagen.flow_from_directory(
        validation_dir,
        target_size=(image_size, image_size),
        batch_size=val_batchsize,
        class_mode='categorical',
        shuffle=False
)

#Read what model to use: base, vgg16 or vgg19 with the third parameter
model_name = "anime_model.h5"
if len(sys.argv) >= 3:
    model_name = sys.argv[2]

model_name, model = load_my_model(model_name)

if model is None:
    print("No model found. Creating model from scratch")

    #create the model depending on the input
    if model_name == "anime_model.h5":
        model = create_model()
    elif model_name ==  "anime_modelvgg16.h5":
        model = create_model_vgg16_base()
    else:
        model = create_model_vgg19_base()

    # Compile the model
    model = compile_model(model)

# Show a summary of the model. Check the number of trainable parameters
model.summary()

#Saving model#################################################################
#define how to save model
filepath = "../models/" + model_name
filepath_all_epochs = "../models/" + model_name + ".all"
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
checkpoint_all_epochs = ModelCheckpoint(filepath_all_epochs, verbose=1)
callbacks_list = [checkpoint, checkpoint_all_epochs]

steps_per_epoch = train_generator.samples // train_generator.batch_size
validation_steps = validation_generator.samples // validation_generator.batch_size

#Train########################################################################

#Read max epochs from command line (if none, the default is 1)
if len(sys.argv) >= 2 and int(sys.argv[1]) > 0:
    print("Max epochs: {}".format(int(sys.argv[1])))
    max_epochs = int(sys.argv[1])
else:
    max_epochs = 1
    print("No epochs received. Defaulting to max_epochs = {}".format(max_epochs))

#compute class weights because of imbalance
class_weights = class_weight.compute_class_weight(
    'balanced',
    np.unique(train_generator.classes),
    train_generator.classes
)

# Train the model
history = model.fit_generator(
    train_generator,
    steps_per_epoch  = steps_per_epoch,
    epochs           = max_epochs,
    validation_data  = validation_generator,
    validation_steps = validation_steps,
    callbacks        = callbacks_list,
    class_weight     = class_weights,
    verbose=1
)

# ##############################################################################
#check performance throughout epochs
acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(len(acc))

plt.plot(epochs, acc, 'b', label='Training acc')
plt.plot(epochs, val_acc, 'r', label='Validation acc')
plt.title('Training and validation accuracy')
plt.legend()

plt.figure()
plt.savefig("accuracy.png")

plt.plot(epochs, loss, 'b', label='Training loss')
plt.plot(epochs, val_loss, 'r', label='Validation loss')
plt.title('Training and validation loss')
plt.legend()

plt.savefig("loss.png")
# plt.show()

##############################################################################

