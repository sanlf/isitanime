from keras import models
from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array

from sklearn.metrics import classification_report, confusion_matrix

import matplotlib.pyplot as plt
import numpy as np

import os
import sys

from my_models import create_model, create_model_vgg16_base, create_model_vgg19_base, load_my_model

##############################################################################

image_size = 224
batch_size = 8
prediction_dir = "predictions"

##############################################################################

def preprocess_image(filename):
    img = load_img(filename, target_size=(image_size, image_size))
    arr = img_to_array(img)
    arr /= 255.
    arr = np.expand_dims(arr, axis = 0)

    return arr

##############################################################################

if __name__ == "__main__":
    #Read what model to use: base, vgg16 or vgg19 with the second parameter
    model_name = "anime_model.h5"
    if len(sys.argv) >= 2:
        model_name = sys.argv[1]

    _, model = load_my_model(model_name)

    if model is None:
        print("No model found!.")

    # Show a summary of the model. Check the number of trainable parameters
    model.summary()

##############################################################################

    # Predict one by one the images in the predictions directory just to visualy
    # see the predictions of the neural network
    for img in os.listdir(prediction_dir):
        img_path = prediction_dir + "/" + img
        img = preprocess_image(prediction_dir + "/" + img)

        prediction = model.predict(img)

        original = load_img(img_path)
        plt.figure(figsize=[7,7])
        plt.axis('off')
        plt.title("Image predictions: anime:{:.2f} non anime:{:.2f}".format(prediction[0][0], prediction[0][1]))
        plt.imshow(original)
        plt.show()

