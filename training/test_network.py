from keras import models
from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array

from sklearn.metrics import classification_report, confusion_matrix

import matplotlib.pyplot as plt
import numpy as np
import itertools

import random

import sys

from my_models import create_model, create_model_vgg16_base, create_model_vgg19_base, load_my_model

##############################################################################

image_size = 224
batch_size = 32

#by default use the test images unless validation is specified
images_dir = "../data/test"
if len(sys.argv) >= 2:
    if sys.argv[1] == "validation":
        images_dir = "../data/validation"

print("Images dir: " + images_dir)
##############################################################################

def preprocess_image(filename):
    img = load_img(filename, target_size=(image_size, image_size))
    arr = img_to_array(img)
    arr /= 255.
    arr = np.expand_dims(arr, axis = 0)

    return arr

##############################################################################

#Read what model to use: base, vgg16 or vgg19 with the second parameter
model_name = "anime_model.h5"
if len(sys.argv) >= 3:
    if ".h5" not in sys.argv[2]:
        model_name = sys.argv[2]
        _, model = load_my_model(model_name)
    else:
        model_name = sys.argv[2]
        model = models.load_model("../models/" + model_name)
else:
    _, model = load_my_model(model_name)




if model is None:
    print("No model found!.")

# Show a summary of the model. Check the number of trainable parameters
model.summary()

##############################################################################

# Create a generator for prediction
images_datagen = ImageDataGenerator(rescale=1./255)
images_generator = images_datagen.flow_from_directory(
        images_dir,
        target_size=(image_size, image_size),
        batch_size=batch_size,
        class_mode='categorical',
        shuffle=False)

# Get the filenames from the generator
fnames = images_generator.filenames

# Get the ground truth from generator
ground_truth = images_generator.classes

# Get the label to class mapping from the generator
label2index = images_generator.class_indices

# Getting the mapping from class index to class label
idx2label = dict((v,k) for k,v in label2index.items())

# by default the step number is the number of samples.
# If the network test is for the validation datset, then
# reduce the step number to agilize the process
steps = images_generator.samples
if len(sys.argv) >= 2:
    if sys.argv[1] == "validation":
        steps = images_generator.samples // images_generator.batch_size
# Get the predictions from the model using the generator
predictions = model.predict_generator(
    images_generator,
    steps = steps,
    verbose=1
)
predicted_classes = np.argmax(predictions,axis=1)

#trim the bigger list to be able to print results
trimlength = min(len(ground_truth), len(predicted_classes))
ground_truth = ground_truth[:trimlength]
predicted_classes = predicted_classes[:trimlength]

errors = np.where(predicted_classes != ground_truth)[0]
corrects = np.where(predicted_classes == ground_truth)[0]
print("No of errors = {}/{}".format(len(errors),images_generator.samples))

##############################################################################
#Confusion matrix

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()

target_names = ['anime', 'non_anime']

cm = confusion_matrix(ground_truth, predicted_classes)

plt.figure()
plot_confusion_matrix(cm, target_names)
plt.savefig("confusion_matrix.png")

print("Classification report")
print(classification_report(
    ground_truth,
    predicted_classes,
    target_names = target_names)
)
##############################################################################

# Show randomly 5 misclassification
random.shuffle(errors)
for i in range(len(errors[:2])):
    pred_class = np.argmax(predictions[errors[i]])
    pred_label = idx2label[pred_class]

    title = 'Original label:{}, Prediction :{}, confidence : {:.3f}'.format(
        fnames[errors[i]].split('/')[0],
        pred_label,
        predictions[errors[i]][pred_class])

    original = load_img('{}/{}'.format(images_dir,fnames[errors[i]]))
    plt.figure(figsize=[7,7])
    plt.axis('off')
    plt.title(title)
    plt.imshow(original)

    plt.savefig("{}-{}-error.png".format(i,
        predictions[errors[i]][pred_class])
    )

# Show randomly 5 correct classification
random.shuffle(corrects)
for i in range(len(corrects[:2])):
    pred_class = np.argmax(predictions[corrects[i]])
    pred_label = idx2label[pred_class]

    title = 'Original label:{}, Prediction :{}, confidence : {:.3f}'.format(
        fnames[corrects[i]].split('/')[0],
        pred_label,
        predictions[corrects[i]][pred_class])

    original = load_img('{}/{}'.format(images_dir,fnames[corrects[i]]))
    plt.figure(figsize=[7,7])
    plt.axis('off')
    plt.title(title)
    plt.imshow(original)

    plt.savefig("{}-{}-correct.png".format(i,
        predictions[errors[i]][pred_class])
    )

##############################################################################

