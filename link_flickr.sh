#!/bin/bash

#Creates a symlink from the scraped images to a data folder
#divided in validation and training

image_number=0
directory=scraper/flickr/non_anime_images
total_images=$(ls -1 $directory | wc -l)
mkdir -p "data/non_anime"

echo "Creating symbolic links"
for image in "$directory"/*;
do
    #echo this to have an idea of progress
    if (( ($image_number % 1000) == 0 ));
    then
        echo "$image_number"
    fi

    ln -rs "$image" "data/non_anime/"

    ((image_number++))
done

test_number=$((total_images * 5 / 100)) #5% of images
validation_number=$((total_images * 25 / 100)) #25% of images
train_number=$((total_images - validation_number - test_number)) #remaining images, about 70% due to rounding

test_dir="data/test/non_anime"
validation_dir="data/validation/non_anime"
train_dir="data/train/non_anime"

mkdir -p "$test_dir"
mkdir -p "$validation_dir"
mkdir -p "$train_dir"

echo "Moving $test_number files to $test_dir"
find data/non_anime -type l | head -$test_number | xargs -I {} mv {} "$test_dir"

echo "Moving $validation_number files to $validation_dir"
find data/non_anime -type l | head -$validation_number | xargs -I {} mv {} "$validation_dir"

echo "Moving $train_number files to $train_dir"
find data/non_anime -type l | head -$train_number | xargs -I {} mv {} "$train_dir"

echo "Deleting data/non_anime directory"
#rm -r data/non_anime

