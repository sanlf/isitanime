# IsItAnime
## English
Project based on [Antonpaquin's github project](https://github.com/antonpaquin/IsItAnime).
Trains a neural network for binary classification to detect if an input image is anime
or not.
  
  
Visit [this github page](sanlf.github.io) to know about some experiences with the project (currently
only in spanish).


## Español
Proyecto basado en [el proyecto de github de Antopaquin](https://github.com/antonpaquin/IsItAnime.)
Entrena una red neuronal de clasificación binaria para detectar si una images es anime o no.


visita [esta página de github](sanlf.github.io) para leer sobre algunas experiencias que tuve
al desarrollar el proyecto.

