#!/bin/bash

anime_number=$(($(ls -1 "data/test/anime" | wc -l) + $(ls -1 "data/validation/anime" | wc -l) + $(ls -1 "data/train/anime" | wc -l)))
non_anime_number=$(($(ls -1 "data/test/non_anime" | wc -l) + $(ls -1 "data/validation/non_anime" | wc -l) + $(ls -1 "data/train/non_anime" | wc -l)))

test_anime_number=$(ls -1 "data/test/anime" | wc -l)
validation_anime_number=$(ls -1 "data/validation/anime" | wc -l)
train_anime_number=$(ls -1 "data/train/anime" | wc -l)

test_non_anime_number=$(ls -1 "data/test/non_anime" | wc -l)
validation_non_anime_number=$(ls -1 "data/validation/non_anime" | wc -l)
train_non_anime_number=$(ls -1 "data/train/non_anime" | wc -l)

diff_test=$((test_non_anime_number - test_anime_number))
diff_validation=$((validation_non_anime_number - validation_anime_number))
diff_train=$((train_non_anime_number - train_anime_number))

if (( diff_test >= 0 ));
then
    test_surplus_directory="data/test/non_anime"
else
    test_surplus_directory="data/test/anime"
fi

if (( diff_validation >= 0 ));
then
    validation_surplus_directory="data/validation/non_anime"
else
    validation_surplus_directory="data/validation/anime"
fi

if (( diff_train >= 0 ));
then
    train_surplus_directory="data/train/non_anime"
else
    train_surplus_directory="data/train/anime"
fi

#take absolute value of differences
diff_test=${diff_test#-}
diff_validation=${diff_validation#-}
diff_train=${diff_train#-}

find "data/test/non_anime" -type l | sort -R | head -$diff_test | xargs rm
find "data/validation/non_anime" -type l | sort -R | head -$diff_validation | xargs rm
find "data/train/non_anime" -type l | sort -R | head -$diff_train | xargs rm

find "$test_surplus_directory" -type l | sort -R | head -$diff_test | xargs rm
find "$validation_surplus_directory" -type l | sort -R | head -$diff_validation | xargs rm
find "$train_surplus_directory" -type l | sort -R | head -$diff_train | xargs rm

