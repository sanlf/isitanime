#!/bin/bash

#Creates a symlink from the scraped images to a data folder
#divided in test, validation and training

image_number=0
directory=scraper/safebooru/anime_images
total_images=$(ls -1 $directory | wc -l)
mkdir -p "data/anime"

echo "Creating symbolic links"
for image in "$directory"/*;
do
    #echo this to have an idea of progress
    if (( ($image_number % 1000) == 0 ));
    then
        echo "$image_number"
    fi

    ln -rs "$image" "data/anime/"

    ((image_number++))
done

echo "Removing all gif files"
rm "data/anime"/*.gif

test_number=$((total_images * 5 / 100)) #5% of images
validation_number=$((total_images * 25 / 100)) #25% of images
train_number=$((total_images - validation_number - test_number)) #remaining images, about 70% due to rounding

test_dir="data/test/anime"
validation_dir="data/validation/anime"
train_dir="data/train/anime"

mkdir -p "$test_dir"
mkdir -p "$validation_dir"
mkdir -p "$train_dir"

echo "Moving $test_number files to $test_dir"
mv -v $(find data/anime -type l | head -$test_number) "$test_dir"

echo "Moving $validation_number files to $validation_dir"
mv -v $(find data/anime -type l | head -$validation_number) "$validation_dir"

echo "Moving $train_number files to $train_dir"
mv -v $(find data/anime -type l | head -$train_number) "$train_dir"

echo "Deleting data/anime directory"
rm -r data/anime

